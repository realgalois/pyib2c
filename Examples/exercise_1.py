import pyib2c
import time
import numpy as np
import os
import inspect


# ------------------ Network creation ----------------------

network_behavior = pyib2c.Network.NetworkBehaviorClass()

# Add the information of the input and output
network_behavior.input_info = {'Size': np.array([]),
                               'Type': np.array([]),
                               'Description': []}

network_behavior.output_info = {'Size': np.array([]),
                                'Type': np.array([]),
                                'Description': []}

# Specify number of behaviors

# Generate matrices and references
network_behavior.generate()

# ---- Configure connections ----
# list_of_variables = [[Variable, Position],[]]


# ---- Finish network ----
network_behavior.finish_network()

# ---- Give to the modules specific values ----


# ----------------------------------------------------------

# Store the complete behavioral network


# Simulation

t_ini = 0
t_end = 1000
delta_t = 0.1

pose = pyib2c.Tools.Agents.PoseClasses.PoseV1Class()
dyn = pyib2c.Tools.Agents.DynClasses.DynV2Class(pose, tau_s=1)

simulator = pyib2c.Tools.Simulator.Simulator2D(pose)
visualization = pyib2c.Tools.Visualization.VisualizationClasses.Visualization(simulator=simulator)

t = 0.0
t_real = time.time()
while t <= t_end:

    # Sensors
    sensed_data = simulator.sensed_data

    # Execute network
    target = np.zeros([1, 2])
    target[0][0] = sensed_data.targets[0][0]
    target[0][1] = sensed_data.targets[0][1]
    sensors_state = sensed_data.state_readings[0]  # Pass only the readings of one agent
    inputs_network = np.array([t, pose, target, sensors_state])
    network_behavior.calc_outputs(inputs_network=inputs_network)

    # Retrieve commanded velocity
    V_c = network_behavior.get_output()[0]
    activity = network_behavior.get_activity()
    rating = network_behavior.get_rating()

    # Calculate new pose
    pose = dyn.update_pose(V_c[0:2], t)

    # Simulation
    simulator.step(pose, V_c, t)

    # Visualization
    visualization.update_visualization(simulator=simulator, pose_agent=pose, V_c=V_c)

    # Time step
    t += delta_t

t_real = time.time() - t_real
print('Time compression: ', t_end/t_real)

