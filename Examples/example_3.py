import pyib2c
import time
import numpy as np
import os
import inspect


# ------------------ Network creation ----------------------

network_behavior = pyib2c.Network.NetworkBehaviorClass()

# Add the information of the input and output

network_behavior.input_info = {'Size': np.array([np.zeros([1]),
                                                 pyib2c.Tools.Agents.PoseClasses.PoseV1Class(),
                                                 np.zeros([1, 2]),
                                                 np.zeros([8])]),
                               'Type': np.array(['Time', 'Pose', 'Position2D', 'ProximitySensors']),
                               'Description': ['Simulation time [s]',
                                               'Pose of the agent',
                                               '2D position of the target',
                                               'Proximity measurements, 8 values']}

network_behavior.output_info = {'Size': np.array([np.zeros([2])]),
                                'Type': 'Velocity2D',
                                'Description': '2D Velocity Command'}

# Specify number of behaviors
network_behavior.behaviors_list['AvoidObstaclesBehavior'] = 1
network_behavior.fusion_list['Weighted'] = 1

name = 'network_GoToPoint.ntm'
path = '/home/pablo/ownCloud/SwarmCity/Models/Python/pyib2c/Examples/Networks_saved'
network_behavior.add_external_behavior(name, path)

# Generate matrices and references
network_behavior.generate()

# ---- Configure connections ----
# list_of_variables = [[Variable, Position],[]]
network_behavior.add_connection('I_O', 1, 4, list_variables=[[0, 0], [1, 1], [2, 2]])
network_behavior.add_connection('I_O', 1, 3, list_variables=[[1, 0], [3, 1]])

network_behavior.add_connection('I_O', 3, 2)
network_behavior.add_connection('I_O', 4, 2)

network_behavior.add_connection('I_O', 2, 5)

network_behavior.add_connection('Stimulation', 1, 2)
network_behavior.add_connection('Inhibition', 1, 2)
network_behavior.add_connection('Inhibition', 3, 4)

network_behavior.add_connection('Activity_Network', 2)
network_behavior.add_connection('Rating_Network', 2)

# ---- Finish network ----
network_behavior.finish_network()

# ---- Give to the modules specific values ----
network_behavior.assign_value_to_parameter(3, 'v_n', 2)

# ----------------------------------------------------------

# Store the complete behavioral network
path = os.path.join(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))), 'Networks_saved')
name = 'network_AvoidObstacles'
network_behavior.store_network(path, name)

# Simulation

t_ini = 0
t_end = 1000
delta_t = 0.1

pose = pyib2c.Tools.Agents.PoseClasses.PoseV1Class()
dyn = pyib2c.Tools.Agents.DynClasses.DynV2Class(pose, tau_s=1)

simulator = pyib2c.Tools.Simulator.Simulator2D(pose)
visualization = pyib2c.Tools.Visualization.VisualizationClasses.Visualization(simulator=simulator)

t = 0.0
t_real = time.time()
while t <= t_end:

    # Sensors
    sensed_data = simulator.sensed_data

    # Execute network
    target = np.zeros([1, 2])
    target[0][0] = sensed_data.targets[0][0]
    target[0][1] = sensed_data.targets[0][1]
    sensors_state = sensed_data.state_readings[0]  # Pass only the readings of one agent
    inputs_network = np.array([t, pose, target, sensors_state])
    network_behavior.calc_outputs(inputs_network=inputs_network)

    # Retrieve commanded velocity
    V_c = network_behavior.get_output()[0]

    # Calculate new pose
    pose = dyn.update_pose(V_c[0:2], t)

    # Simulation
    simulator.step(pose, V_c, t)

    # Visualization
    visualization.update_visualization(simulator=simulator, pose_agent=pose, V_c=V_c)

    # Time step
    t += delta_t

t_real = time.time() - t_real
print('Time compression: ', t_end/t_real)

