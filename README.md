## Wellcome to Py-iB2c

This is the Python implementation of the **iB2C** \[[1](http://merkur.informatik.uni-kl.de/fileadmin/Literatur/Proetzsch10.pdf)\] architecture to execute behavioral networks to control intelligent agents.
The library allows to easily create behaviors, implement them in a Network and execute it.
Py-iB2c has been developed for the **[SwarmCity Project](http://www.swarmcityproject.com/)**, at the [Centre for Automation and Robotics](https://www.car.upm-csic.es/), [Technical University of Madrid](http://www.upm.es/internacional).


---


## Py-iB2C

Our goal is to develop an easy-to-use library to implement behavioral networks. If you are willing to participate in the development, you are wellcome to pull a request or to contact with any of the participants of the project.

In the 0.1 version, you can:

1. Create new behaviors from scratch, implementing the transfer, activity and rating functions.
2. Create networks using the already implemented behaviors or new ones.
3. Save specific behaviors and reuse them in another network.
4. Save networks and use them as behavior modules.

Please, refer to the Wiki to learn how to use Py-iB2c, as well as its current limitations.
