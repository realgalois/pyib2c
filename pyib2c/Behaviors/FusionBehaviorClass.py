# Fusion Behavior Class

import numpy as np
import sys
import warnings


class FusionBehaviorClass:
    def __init__(self, b_class='Max', n_inputs=0):

        # Output and input information
        # -------------------------------------------------------
        self.input_info = {'Size': np.array([]),
                           'Type': np.array([]),
                           'Description': []}

        self.output_info = {'Size': np.array([]),
                            'Type': np.array([]),
                            'Description': []}
        # -------------------------------------------------------

        self.b_class = b_class
        self.n_inputs = n_inputs

        # Inputs definition
        self.inputs = np.zeros(n_inputs)
        self.activities_in = np.zeros(n_inputs)
        self.ratings_in = np.zeros(n_inputs)

        # Stimulation, Inhibition and Activation
        self.stimulation = 0
        self.inhibition = 0
        self.activation = 0

        self.output = np.array([np.zeros([2])])
        self.activity = 0
        self.rating = 0

        # Definition of the functions
        if b_class == 'Max':
            self.calc_outputs_selected = self.calc_outputs_max
        elif b_class == 'Weighted':
            self.calc_outputs_selected = self.calc_outputs_weighted
        elif b_class == 'Weighted_max':
            self.calc_outputs_selected = self.calc_outputs_weighted_max
        else:
            sys.exit('---- Fusion module name not recognized: ', b_class, '!!!')

    def calc_outputs(self, input_set=[], stimulations=1, inhibitions=0):

        n_inputs = np.size(input_set, axis=1)
        size_inputs = np.size(input_set[0, 0])

        # Retrieve input information
        self.inputs = np.zeros([n_inputs, size_inputs])
        self.activities_in = np.zeros([n_inputs])
        self.ratings_in = np.zeros([n_inputs])
        for i in range(0, n_inputs):
            self.inputs[i, :] = input_set[0, i]
            self.activities_in[i] = np.clip(np.max(input_set[1, i]), 0, 1)
            self.ratings_in[i] = np.clip(np.max(input_set[2, i]), 0, 1)

        # Calculate activation
        stimulations = np.clip(stimulations, 0, 1)
        inhibitions = np.clip(inhibitions, 0, 1)
        self.activation = np.max(stimulations) * (1 - np.max(inhibitions))

        # Calculate output, activity and rating
        self.calc_outputs_selected()

        # Limit activity and rating
        self.activity = np.clip(self.activity, 0, 1)
        self.rating = np.clip(self.rating, 0, 1)

        return self.output, self.activity, self.rating

    def update_IO_info(self, network, this_ID_in_network):

        ##############################################################################################
        # This function updates the input and output information for this fusion module
        ##############################################################################################

        # Find the number of modules connected to this fusion module
        pos_no_module_inputs = np.where(network.I_O_conn[:, this_ID_in_network-1] == 0)[0]
        n_module_inputs = np.size(network.I_O_conn[:, 0]) - np.size(pos_no_module_inputs)

        self.input_info['Size'] = np.zeros([n_module_inputs], dtype=object)
        self.input_info['Type'] = np.zeros([n_module_inputs], dtype=object)
        self.input_info['Description'] = np.zeros([n_module_inputs], dtype=object)

        cont = 0
        for pos_module in range(np.size(network.I_O_conn[:, 0])):
            if np.where(pos_no_module_inputs == pos_module)[0].size == 0:  # pos_module is an input module
                # Get information of the input module
                ID_input_module = pos_module + 1
                pos_module_in_execution_array = np.where(ID_input_module == network.execution_array[:, 0])[0][0]
                module_output_info = network.execution_array[pos_module_in_execution_array, 1].output_info
                if network.I_O_conn[pos_module, this_ID_in_network-1] == -1:

                    if np.size(module_output_info['Size'], axis=0) > 1:
                        error_msg = 'Network ERROR: fusion module input from ID ' + str(ID_input_module) + ' has more' \
                                    ' than 1 output. Please, specify which output variable must be fused.'
                        raise RuntimeError(error_msg)

                    self.input_info['Size'][cont] = module_output_info['Size'][0]
                    self.input_info['Type'][cont] = module_output_info['Type']
                    self.input_info['Description'][cont] = module_output_info['Description']
                    cont += 1

        # Check if all the inputs have the same size and are of the same type
        size_fusion = self.input_info['Size'][0]
        type_fusion = self.input_info['Type'][0]
        for i in range(1, cont):
            if np.size(size_fusion) != np.size(self.input_info['Size'][i]):
                error_msg = 'Network ERROR: fusion ID ' + str(this_ID_in_network) + ' has different input variable' \
                            ' sizes. Please, check the connections.'
                raise RuntimeError(error_msg)
            elif type_fusion != self.input_info['Type'][i]:
                warning_msg = 'Network WARNING: fusion ID ' + str(this_ID_in_network) + ' has different input ' \
                                'variable Types, but with same size. Consider checking the connections.'
                warnings.warn(warning_msg)

        self.input_info['Type'] = self.input_info['Type'][0]
        self.input_info['Description'] = self.input_info['Description'][0]

        # Fill the output information
        self.output_info['Size'] = np.array([self.input_info['Size'][0]])
        self.output_info['Type'] = self.input_info['Type']
        self.output_info['Description'] = self.input_info['Description']

    def calc_outputs_max(self):

        # Maximum fusion behavior: max activity is selected
        input_max_pos = np.argmax(self.activities_in)
        self.output[0] = self.inputs[input_max_pos, :]
        self.activity = self.activities_in[input_max_pos]
        self.rating = self.ratings_in[input_max_pos]

    def calc_outputs_weighted(self):

        # Weighted fusion behavior: weighted activities
        size_inputs = np.size(self.inputs, axis=1)
        output = np.zeros([size_inputs])
        for i in range(size_inputs):
            output[i] = np.sum(self.inputs[:, i] * self.activities_in)

        self.output[0] = output / np.sum(self.activities_in)
        self.activity = self.activation * self.activation * np.sum(self.activities_in * self.activities_in) / np.sum(
            self.activities_in)
        self.rating = np.sum(self.activities_in * self.ratings_in) / np.sum(self.activities_in)

    def calc_outputs_weighted_max(self):

        # Weighted sum fusion behavior: weighted sum of the activities
        size_inputs = np.size(self.inputs, axis=1)
        output = np.zeros([size_inputs])
        for i in range(size_inputs):
            output[i] = np.sum(self.inputs[:, i] * self.activities_in)

        self.output[0] = output / np.max(self.activities_in)

        self.activity = self.activation * np.min([1,
                                                  np.sum(self.activities_in * self.activities_in) / np.max(
                                                      self.activities_in)])
        self.rating = np.sum(self.activities_in * self.ratings_in) / np.sum(self.activities_in)

    def get_output(self):
        return self.output

    def get_activity(self):
        return np.array([self.activity])

    def get_rating(self):
        return np.array([self.rating])
