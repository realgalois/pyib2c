import numpy as np

from .BehaviorTemplateClass import BehaviorTemplateClass


class EmptyBehaviorClass(BehaviorTemplateClass):

    def __init__(self, ID=1):

        # Output and input information
        # -------------------------------------------------------
        self.input_info = {'Size': np.array([]),
                           'Type': np.array([]),
                           'Description': []}

        self.output_info = {'Size': np.array([]),
                            'Type': np.array([]),
                            'Description': []}
        # -------------------------------------------------------

        # Initialize base behavior class
        BehaviorTemplateClass.__init__(self, ID)

        # Variables for this behavior
        # self._t = 0  Internal use
        # self.P = 0  Parameter

    def calc_transfer(self, inputs):

        self.output[0] = np.array([0])

    def calc_activation(self):

        self.activation = np.array([self.activity])

    def calc_rating(self):

        self.rating = np.array([1])


