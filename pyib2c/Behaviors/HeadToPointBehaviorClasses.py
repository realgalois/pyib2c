import numpy as np

from pyib2c.Tools.Agents.PoseClasses import PoseV1Class
from pyib2c.Behaviors.BehaviorTemplateClass import BehaviorTemplateClass


class HeadToPointBehaviorClass(BehaviorTemplateClass):

    def __init__(self, ID=1):

        # Output and input information
        # -------------------------------------------------------
        self.input_info = {'Size': np.array([np.zeros([1]),
                                             PoseV1Class(),
                                             np.zeros([2]),
                                             np.zeros([1])]),
                           'Type': np.array(['Time', 'Pose', 'Position2D', 'CoefAlpha']),
                           'Description': ['Simulation time [s]',
                                           'Pose of the agent',
                                           '2D position of the target',
                                           'Coefficient [0, 1] of velocity reduction']}

        self.output_info = {'Size': np.array([np.zeros([2])]),
                            'Type': 'Velocity2D',
                            'Description': '2D Velocity Command'}
        # -------------------------------------------------------

        # Initialize base behavior class
        BehaviorTemplateClass.__init__(self, ID)

        # Variables for this behavior
        self._t = 0
        self.P = 10
        self.v_n = 1
        self._delta_psi = 0

    def calc_transfer(self, inputs):

        # Get input variables
        t = inputs[0]
        pose = inputs[1]
        target_pos = inputs[2]
        alpha_v = inputs[3]

        # -------------------------------------------------------
        # Calc output vector with the transfer function
        # -------------------------------------------------------
        i_target = 0
        r_a_t = [target_pos[i_target][0] - pose.X[0], target_pos[i_target][1] - pose.X[1]]
        psi_a_t = np.arctan2(r_a_t[0], r_a_t[1])
        delta_psi = psi_a_t - pose.psi

        # Correction [-pi,+pi]
        self._delta_psi = np.arctan2(np.sin(delta_psi), np.cos(delta_psi))

        # Calculation of the angular velocity
        w = self.P*self._delta_psi/np.pi

        delta_t = t - self._t
        self._t = t

        psi_c = pose.psi + delta_t * w
        # -------------------------------------------------------

        v_c = np.zeros(2)
        v_c[0] = np.sin(psi_c)
        v_c[1] = np.cos(psi_c)
        v_c = alpha_v*self.v_n * v_c

        self.output[0] = v_c

    def calc_activity(self):

        self.activity = np.array([self.activation])

    def calc_rating(self):

        self.rating = np.array([1 - np.abs(self._delta_psi) / np.pi])


