import numpy as np

import itertools
import math
import random

import pygame

import pymunk
from pymunk.vec2d import Vec2d

from pymunk.pygame_util import DrawOptions


class Simulator2D:
    def __init__(self, pose):

        # Initialize variables
        self.WIDTH = 2000
        self.HEIGHT = 900
        self.ROBOT_SIZE = 20
        self.MIN_OBSTACLES = 3
        self.MAX_OBSTACLES = 10
        self.MIN_OBSTACLE_SIZE = 10
        self.MAX_OBSTACLE_SIZE = 100
        self.MIN_OBSTACLE_SPEED = 1
        self.MAX_OBSTACLE_SPEED = 5
        self.TARGETS = 1
        self.THRESHOLD = 10

        self.spread = 10  # Default spread of the sensor

        self.simulation_time = 0
        self.pose = []
        self.pose.append(pose.X[0] + self.WIDTH / 2)
        self.pose.append(pose.X[1] + self.HEIGHT / 2)

        self.draw_screen = True
        self.show_sensors = True

        # Initialize pygame
        self.create_game()

        # Create scenario
        self.create_scenario()

        # Create robot
        self.create_robot()

        # Record steps
        self.num_steps = 0

        # Create obstacles
        self.create_obstacles()

        # Create targets
        self.create_targets()

        # Create sensed_data object
        self.sensed_data = SensedDataClass(self.TARGETS)
        self.sensed_data.update_sensed_data(self.targets, self.WIDTH, self.HEIGHT, np.array([np.ones([8])]))

    # Create game
    def create_game(self):

        pygame.init()
        self.screen = pygame.display.set_mode([self.WIDTH, self.HEIGHT])
        self.screen.set_alpha(None)

        self.clock = pygame.time.Clock()
        self.crashed = False

        # Evaluation
        self.distance = []
        self.scores = []
        self.time = []
        self.collision = []

    # Create scenario
    def create_scenario(self):

        # Physics
        self.space = pymunk.Space()
        self.space.gravity = pymunk.Vec2d(0.0, 0.0)

        # Create walls
        static = [
            pymunk.Segment(
                self.space.static_body,
                (0, 1), (0, self.HEIGHT), 1),
            pymunk.Segment(
                self.space.static_body,
                (1, self.HEIGHT), (self.WIDTH, self.HEIGHT), 1),
            pymunk.Segment(
                self.space.static_body,
                (self.WIDTH-1, self.HEIGHT), (self.WIDTH-1, 1), 1),
            pymunk.Segment(
                self.space.static_body,
                (1, 1), (self.WIDTH, 1), 1)
        ]

        for s in static:
            s.friction = 1.
            s.group = 1
            s.collision_type = 1
            s.color = (255, 0, 0)

        self.space.add(static)

    # Create robot
    def create_robot(self):

        x = self.pose[0]  # + self.WIDTH/2
        y = self.pose[1]  # + self.HEIGHT/2
        r = 0.0

        self.robot_body = pymunk.Body(mass=100, moment=100)
        self.robot_body.position = x, y
        self.robot_shape = pymunk.Poly(self.robot_body, [(0.8 * self.ROBOT_SIZE * math.cos(math.pi/6), 0),
                                                         (-0.2 * self.ROBOT_SIZE * math.cos(math.pi/6),
                                                          -0.8 * self.ROBOT_SIZE * math.sin(math.pi/6)),
                                                         (-0.2 * self.ROBOT_SIZE * math.cos(math.pi/6),
                                                          0.8 * self.ROBOT_SIZE * math.sin(math.pi/6))])

        self.robot_shape.color = (0, 200, 0, 0)
        self.robot_shape.elasticity = 1.0
        self.robot_body.angle = r
        move_direction = Vec2d(1, 0).rotated(self.robot_body.angle)
        self.robot_body.apply_impulse_at_local_point(move_direction)
        self.space.add(self.robot_body, self.robot_shape)

    # Create targets:
    # - It generates a number TARGETS of targets that must be reached by the robot.
    # - It is called at the beginning of the game or when all the targets are reached.
    def create_targets(self):

        # Array of targets
        self.targets = []

        t = 0

        # Create targets
        while t < self.TARGETS:

            # Target location
            x = int(self.WIDTH/2 + np.random.randint(-self.WIDTH/3, self.WIDTH/3))
            y = int(self.HEIGHT/2 + np.random.randint(-self.HEIGHT/3, self.HEIGHT/3))
            target = (x, y)

            # Target sprite
            tar_sprite = pygame.sprite.Sprite()
            tar_sprite.radius = self.THRESHOLD
            tar_sprite.position = x, y
            tar_sprite.rect = pygame.Rect(x - self.THRESHOLD, y - self.THRESHOLD, self.THRESHOLD, self.THRESHOLD)

            # Check collisions
            collision = False

            for obstacle in self.obstacles:

                # Obstacle sprite
                obs_sprite = pygame.sprite.Sprite()
                obs_sprite.radius = obstacle.mass
                obs_sprite.position = obstacle.position
                obs_sprite.rect = pygame.Rect(obstacle.position[0]-obstacle.mass, obstacle.position[1]-obstacle.mass, 2*(obstacle.mass), 2*(obstacle.mass))

                # Invalid target
                if pygame.sprite.collide_circle(tar_sprite, obs_sprite) == True:
                    collision = True

            # Valid target
            if collision == False:
                self.targets.append(target)
                t = t + 1

        # Evaluation
        self.distance.append(self.calculateShortestDistance(self.targets))
        self.time0 = self.simulation_time
        self.collision.append(0)

    # Create obstacles:
    def create_obstacles(self):
        # Create obstacles
        self.obstacles = []

        num = np.random.randint(self.MIN_OBSTACLES, self.MAX_OBSTACLES)

        for n in range(0, num):
            type = np.random.randint(0, 2)
            x = np.random.randint(0, self.WIDTH)
            y = np.random.randint(0, self.HEIGHT)
            r = np.random.randint(self.MIN_OBSTACLE_SIZE, self.MAX_OBSTACLE_SIZE)

            obstacle = self.create_obstacle(type, x, y, r)

            self.obstacles.append(obstacle)

    def create_obstacle(self, type, x, y, r):
        c_body = pymunk.Body(mass=r, moment=r)
        if type == 0:
            c_shape = pymunk.Circle(c_body, r)
        else:
            c_shape = pymunk.Poly(c_body, [(-r, -r), (-r, r), (r, r), (r, -r)])
        c_shape.elasticity = 0.0
        c_body.position = x, y
        c_shape.color = (0, 0, 255)
        self.space.add(c_body, c_shape)
        return c_body

    def step(self, pose, V_c, time):

        self.simulation_time = time

        # Update agent position
        self.pose[0] = pose.X[0] + self.WIDTH/2
        self.pose[1] = pose.X[1] + self.HEIGHT/2
        self.robot_body.position = pose.X[0] + self.WIDTH/2, pose.X[1] + self.HEIGHT/2
        self.robot_body.velocity = Vec2d(pose.V[0], pose.V[1])
        self.robot_body.angle = np.pi/2 - pose.psi

        # Draw background:
        color_background = (250, 250, 250, 255)
        self.screen.fill(color_background)

        # Draw space:
        draw = DrawOptions(self.screen)
        self.space.debug_draw(draw)

        # Update scenario
        self.move_obstacles()
        self.check_targets()

        # Check collisions:
        agent_crashed = self.check_collisions()  # self.agent_is_crashed(readings)
        if agent_crashed:
            self.collision[len(self.collision) - 1] = self.collision[len(self.collision) - 1] + 1
            print("Collision")

        self.space.step(1./10)
        if self.draw_screen:
            pygame.display.flip()

        # Robot sensing
        x, y = self.robot_body.position[0], self.robot_body.position[1]
        readings = self.get_sonar_readings(x, y, self.robot_body.angle)
        normalized_readings = []
        for reading in readings:
            normalized_readings.append(reading / 4)  # ((reading - 20.0) / 20)
        state_readings = np.array([normalized_readings])

        self.sensed_data.update_sensed_data(self.targets, self.WIDTH, self.HEIGHT, state_readings)

        self.clock.tick()

        return state_readings, agent_crashed

    def move_obstacles(self):
        # Randomly move obstacles around.
        ind = 1
        for obstacle in self.obstacles:
            if ind > np.size(self.obstacles, axis=0)/2:
                speed = random.randint(self.MIN_OBSTACLE_SPEED, self.MAX_OBSTACLE_SPEED)
                direction = Vec2d(1, 0).rotated(self.robot_body.angle + random.randint(-2, 2))
                obstacle.velocity = speed * direction
            ind = ind + 1

    def calculateDistance(self, p1, p2):
        return math.sqrt(math.pow(p2[0] - p1[0], 2) + math.pow(p2[1] - p1[1], 2))

    def calculateShortestDistance(self, targets):

        # Initialize shortest distance to high number
        minDistance = 1000000

        # One target
        if len(targets) == 1:
            target_mod = targets[0]
            target_mod = (target_mod[0], self.HEIGHT - target_mod[1])
            minDistance = self.calculateDistance(self.pose, target_mod)
            return minDistance

        # Multiple targets

        # Get permutations
        numbers = np.arange(0, len(targets))
        permutations = list(itertools.permutations(numbers))

        # Evaluate permutations
        for i in range(len(permutations)):
            distance = self.calculateDistance(self.pose, targets[permutations[i][0]])

            # Compute distance
            for j in range(1, len(permutations[i])):
                distance = distance + self.calculateDistance(targets[j-1], targets[j])

            # Keep shortest distance
            if distance < minDistance:
                minDistance = distance

        return minDistance

    # Check targets:
    # - Check if the robot has reached targets and, in this case, generates new targets.
    def check_targets(self):
        xr = self.robot_body.position[0]
        #yr = self.robot_body.position[1]
        yr = self.HEIGHT - self.robot_body.position[1]
        pr = (xr, yr)
        for target in self.targets:
            xt = target[0]
            yt = target[1]
            pt = (xt, yt)
            pygame.draw.circle(self.screen, (255, 0, 0, 0), [xt, yt], 10)
            if self.calculateDistance(pr, pt) < self.THRESHOLD:
                self.targets.remove(target)
        if len(self.targets) == 0:
            self.time.append(self.simulation_time - self.time0)
            score = self.distance[-1] / self.time[-1]
            self.scores.append(score)

            print("Distance: ", end=" ")
            print(self.distance[-1], end=" ")
            print("- Time: ", end=" ")
            print(self.time[-1], end=" ")
            print("- Score: ", end=" ")
            print(self.scores[-1], end=" ")

            print("- Collisions: ", end= " ")
            print(self.collision[-1])
            self.create_targets()

            print('Total time: ', self.simulation_time)

    def check_collisions(self):

        # Robot sprite
        rob_sprite = pygame.sprite.Sprite()
        rob_sprite.radius = self.THRESHOLD
        rob_sprite.position = self.robot_body.position[0], self.robot_body.position[1]
        rob_sprite.rect = pygame.Rect(rob_sprite.position[0] - self.THRESHOLD/2, rob_sprite.position[1] - self.THRESHOLD/2, self.THRESHOLD, self.THRESHOLD)

        # Check collisions
        collision = False

        for obstacle in self.obstacles:

            # Obstacle sprite
            obs_sprite = pygame.sprite.Sprite()
            obs_sprite.radius = obstacle.mass
            obs_sprite.position = obstacle.position
            obs_sprite.rect = pygame.Rect(obstacle.position[0]-obstacle.mass, obstacle.position[1]-obstacle.mass, 2*obstacle.mass, 2*obstacle.mass)


            if pygame.sprite.collide_circle(rob_sprite, obs_sprite) == True:
                    collision = True

        # Valid target
        if collision == False:
            return False
        else:
            return True

    def sum_readings(self, readings):
        """Sum the number of non-zero readings."""
        tot = 0
        for i in readings:
            tot += i
        return tot

    def get_sonar_readings(self, x, y, angle):
        readings = []
        # Make our arms.
        arm_back = self.make_sonar_arm(x, y)
        arm_left_back = arm_back
        arm_left = arm_back
        arm_left_front = arm_back
        arm_front = arm_back
        arm_right_front = arm_back
        arm_right = arm_back
        arm_right_back = arm_back

        # Rotate them and get readings.
        self.sensor_points = []
        for ind in range(0, 9 * 4):
            self.sensor_points.append((0, 0))

        readings.append(self.get_arm_distance(arm_front, x, y, angle, 0, 1))
        readings.append(self.get_arm_distance(arm_right_front, x, y, angle, -math.pi / 4, 2))
        readings.append(self.get_arm_distance(arm_right, x, y, angle, -math.pi / 2, 3))
        readings.append(self.get_arm_distance(arm_right_back, x, y, angle, -3 * math.pi / 4, 4))
        readings.append(self.get_arm_distance(arm_back, x, y, angle, -math.pi, 5))
        readings.append(self.get_arm_distance(arm_left_back, x, y, angle, -5 * math.pi / 4, 6))
        readings.append(self.get_arm_distance(arm_left, x, y, angle, -3 * math.pi / 2, 7))
        readings.append(self.get_arm_distance(arm_left_front, x, y, angle, -7 * math.pi / 4, 8))

        if self.show_sensors:
            pygame.display.update()

        return readings

    def get_arm_distance(self, arm, x, y, angle, offset, ind):
        # Used to count the distance.
        i = 0

        # Look at each point and see if we've hit something.
        for point in arm:
            i += 1

            # Move the point to the right spot.
            rotated_p = self.get_rotated_point(x, y, point[0], point[1], angle + offset)

            self.sensor_points[(ind-1) * 4 + i] = rotated_p

            # Check if we've hit something. Return the current i (distance)
            # if we did.
            if rotated_p[0] <= 0 or rotated_p[1] <= 0 or rotated_p[0] >= self.WIDTH or rotated_p[1] >= self.HEIGHT:
                return i  # Sensor is off the screen.
            else:
                obs = self.screen.get_at(rotated_p)

                if self.show_sensors:
                    sensor_color = (0, 0, 0, 0)
                    pygame.draw.circle(self.screen, sensor_color, (rotated_p), 2)

                if self.get_track_or_not(obs) != 0:
                    return i

        return i

    def make_sonar_arm(self, x, y):
        distance = 10  # Gap before first sensor.
        arm_points = []

        for i in range(1, 5):
            arm_points.append((distance + x + (self.spread * i), y))

        return arm_points

    def get_rotated_point(self, x_1, y_1, x_2, y_2, radians):
        # Rotate x_2, y_2 around x_1, y_1 by angle.
        x_change = (x_2 - x_1) * math.cos(radians) + \
            (y_2 - y_1) * math.sin(radians)
        y_change = (y_1 - y_2) * math.cos(radians) - \
            (x_1 - x_2) * math.sin(radians)
        new_x = x_change + x_1
        new_y = self.HEIGHT - (y_change + y_1)
        return int(new_x), int(new_y)

    def get_track_or_not(self, reading):
        if reading == (250, 250, 250, 255) or reading == (255, 0, 0, 255):
            return 0
        else:
            return 1

class SensedDataClass:

    def __init__(self, n_targets):

        self.n_targets = n_targets
        self.targets = np.zeros([n_targets, 2])
        self.state_readings = np.zeros([8])

    def update_sensed_data(self, targets, width, height, state_readings):

        for i in range(len(targets)):
            self.targets[i, 0] = targets[i][0] - width/2
            self.targets[i, 1] = height/2 - targets[i][1]

        self.state_readings = state_readings

