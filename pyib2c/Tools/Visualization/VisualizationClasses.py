# Classes to visualize the movement of the agent

import numpy as np
import math

import pygame

from pymunk.pygame_util import DrawOptions

from pyib2c.Tools.Agents.PoseClasses import PoseV1Class
from pyib2c.Tools.Simulator.Simulator_2D_V1 import Simulator2D


class Visualization:
    def __init__(self, online_visualization=True,
                 A_s=np.array([[0, 100], [0, 100]]),
                 simulator=Simulator2D(PoseV1Class()),
                 pose_agent=PoseV1Class()):

        self.online_visualization = online_visualization

        if online_visualization:

            # Configure screen
            self.screen = pygame.display.set_mode([simulator.WIDTH, simulator.HEIGHT])
            self.screen.set_alpha(None)

            # Configure visualization
            self.show_sensors = True
            self.draw_screen = True

    def update_visualization(self, simulator, pose_agent, V_c):

        if self.online_visualization:

            # Draw background:
            color_background = (250, 250, 250, 255)
            self.screen.fill(color_background)

            # Draw space:
            draw = DrawOptions(self.screen)
            simulator.space.debug_draw(draw)

            # Draw speed
            self.draw_arrow(simulator, V_c)

            # Draw sensors
            sensor_color = (0, 0, 0, 0)
            for point in simulator.sensor_points:
                pygame.draw.circle(self.screen, sensor_color, point, 2)

            # Draw targets
            target_color = (255, 0, 0, 0)
            for target in simulator.targets:
                pygame.draw.circle(self.screen, target_color, target, 10)

            # Draw screen
            if self.draw_screen:
                pygame.display.flip()

            # plt.plot
            # plt.plot(pose_agent.X[0], pose_agent.X[1], 'bo')
            # plt.pause(0.05)

    def draw_arrow(self, simulator, V_c):

        len_arrow = 50
        angle_V_c = np.pi/2 - np.arctan2(V_c[0], V_c[1])
        delta_pos = [len_arrow*np.cos(angle_V_c), -len_arrow*np.sin(angle_V_c)]

        start_arrow = [simulator.robot_body.position[0], simulator.HEIGHT - simulator.robot_body.position[1]]
        end_arrow = [simulator.robot_body.position[0] + delta_pos[0], simulator.HEIGHT - simulator.robot_body.position[1] + delta_pos[1]]

        color_arrow = (0, 0, 255, 0)
        size_arrow = 5
        pygame.draw.line(self.screen, color_arrow, start_arrow, end_arrow, 5)
        rotation = math.degrees(math.atan2(start_arrow[1] - end_arrow[1], end_arrow[0] - start_arrow[0])) + 90
        pygame.draw.polygon(self.screen, color_arrow, (
        (end_arrow[0] + size_arrow * math.sin(math.radians(rotation)), end_arrow[1] + size_arrow * math.cos(math.radians(rotation))),
        (end_arrow[0] + size_arrow * math.sin(math.radians(rotation - 120)), end_arrow[1] + size_arrow * math.cos(math.radians(rotation - 120))),
        (end_arrow[0] + size_arrow * math.sin(math.radians(rotation + 120)), end_arrow[1] + size_arrow * math.cos(math.radians(rotation + 120)))))


