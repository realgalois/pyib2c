# Class for the pose of the agents

import numpy as np


class PoseV1Class:
    #   2D
    def __init__(self):
        self.X = np.array([0.0, 0.0])
        self.V = np.array([0.0, 0.0])
        self.psi = np.pi

    def calc_psi(self):
        self.psi = np.arctan2(self.V[0], self.V[1])

